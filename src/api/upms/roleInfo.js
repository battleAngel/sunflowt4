import request from '@/utils/request'

export const roleService = {
  list: function(params) {
    return request({
      url: '/role/list',
      method: 'get',
      params
    })
  },
  edit: function(params) {
    return request({
      url: '/role/info',
      method: 'get',
      params
    })
  },
  delete: function(params) {
    return request({
      url: '/role/delete/' + params.roleCode,
      method: 'get'
    })
  },
  save: function(params) {
    return request({
      url: '/role/save',
      method: 'get',
      data: params
    })
  },
  listAllRoles: function() {
    return request({
      url: '/role/all',
      method: 'get'
    })
  }
}

